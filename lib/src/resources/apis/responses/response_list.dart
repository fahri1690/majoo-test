import 'response_body_list.dart';

class ResponseList {
  int statusCode;
  ResponseBodyList responseBodyList;

  ResponseList({this.statusCode, this.responseBodyList});
}