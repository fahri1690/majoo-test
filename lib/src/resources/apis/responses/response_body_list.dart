class ResponseBodyList {
  String message;
  List<dynamic> data;

  ResponseBodyList({this.message, this.data});

  factory ResponseBodyList.fromJson(Map<String, dynamic> json) {
    return ResponseBodyList(
      message: json['message'] as String,
      data: json['data'] as List<dynamic>,
    );
  }

  factory ResponseBodyList.fromJsonAimart(Map<String, dynamic> json) {
    return ResponseBodyList(
      message: json['message'] as String,
      data: json['data'] as List<dynamic>,
    );
  }
}
