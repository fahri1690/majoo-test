class ResponseBodyMessage {
  String message;

  ResponseBodyMessage({this.message});

  factory ResponseBodyMessage.fromJson(Map<String, dynamic> json) {
    return ResponseBodyMessage(
      message: json['message'] as String
    );
  }
}
