import 'response_body_message.dart';

class ResponseMessage {
  int statusCode;
  ResponseBodyMessage responseBodyMessage;

  ResponseMessage({this.statusCode, this.responseBodyMessage});
}