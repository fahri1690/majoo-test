import 'response_body.dart';

class Response {
  int statusCode;
  ResponseBody responseBody;

  Response({this.statusCode, this.responseBody});
}