class ResponseBody {
  String message;
  dynamic data;

  ResponseBody({this.message, this.data});

  factory ResponseBody.fromJson(Map<String, dynamic> json) {
    return ResponseBody(
      message: json['message'] as String,
      data: json['data'] as dynamic,
    );
  }
}
